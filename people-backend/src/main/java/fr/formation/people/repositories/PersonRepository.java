package fr.formation.people.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.people.models.Person;

public interface PersonRepository extends JpaRepository<Person, Integer> {

}
