package fr.formation.people.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data()
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Person {

	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private int id;
	
	private String firstname;
	
}
