export class Person {
    private _id: number;
    private _firstname: String;

    constructor();
    constructor(firstname?: String) {
        if (firstname)
            this._firstname = firstname;
    }

    public get id(): number {
        return this._id;
    }

    public set id(id: number) {
        this._id = id;
    }

    public get firstname(): String {
        return this._firstname;
    }

    public set firstname(firstname: String) {
        this._firstname = firstname;
    }

}