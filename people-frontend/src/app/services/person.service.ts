import { Injectable } from '@angular/core';
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Person } from '../models/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private url: string = environment.webserviceUrl + '/persons';
  private hc: HttpClient;

  constructor(hc: HttpClient) {
    this.hc = hc;
  }

  findAll(): Observable<Person[]> {
    return this.hc.get<any>(this.url).pipe(map((hal) => hal._embedded.persons));
  }

  findById(id: number): Observable<Person> {
    return this.hc.get<Person>(this.url + '/' + id);
  }

  save(g: Person): Observable<any> {
    return this.hc.post<Person>(this.url, JSON.stringify(g));
  }

  update(g: Person): Observable<any> {
    return this.hc.put<Person>(this.url + '/' + g.id, JSON.stringify(g));
  }

  delete(g: Person): Observable<Person> {
    return this.deleteById(g.id);
  }

  deleteById(id: number): Observable<Person> {
    return this.hc.delete<Person>(this.url + '/' + id);
  }


}
