import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/models/person';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.css']
})
export class PersonsComponent implements OnInit {

  persons: Person[];

  constructor(private ps: PersonService) { }

  ngOnInit() {
    this.ps.findAll().subscribe(pl => {
      this.persons = pl;
      console.log(this.persons);
    });
  }

}
