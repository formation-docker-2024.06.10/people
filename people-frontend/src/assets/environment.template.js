(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["BACKEND_URL"] = "${BACKEND_URL}";
  window["env"]["KEYCLOAK_ISSUER"] = "${KEYCLOAK_ISSUER}";
  window["env"]["KEYCLOAK_REALM"] = "${KEYCLOAK_REALM}";
  window["env"]["KEYCLOAK_CLIENTID"] = "${KEYCLOAK_CLIENTID}";
})(this);