(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["BACKEND_URL"] = "http://localhost:8081";
  window["env"]["KEYCLOAK_ISSUER"] = "http://localhost:8085/auth/";
  window["env"]["KEYCLOAK_REALM"] = "People";
  window["env"]["KEYCLOAK_CLIENTID"] = "people-frontend";
})(this);