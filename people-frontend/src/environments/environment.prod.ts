export const environment = {
  production: true,
  webserviceUrl: "http://localhost:8081",
  keycloak: {
    issuer: "http://localhost:8180/auth/",
    realm: "People",
    clientId: "people-frontend"
  }
};
