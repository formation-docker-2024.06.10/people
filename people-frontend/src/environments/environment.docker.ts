export const environment = {
  production: true,
  webserviceUrl: window["env"]["BACKEND_URL"] || "http://localhost:8081",
  keycloak: {
    issuer: window["env"]["KEYCLOAK_ISSUER"] || "http://localhost:8084/auth/",
    realm: window["env"]["KEYCLOAK_REALM"] || "People",
    clientId: window["env"]["KEYCLOAK_CLIENTID"] || "people-frontend"
  }
};
